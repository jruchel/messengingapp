package jruchel.messengingapp.Utils;

import androidx.appcompat.app.AppCompatActivity;

public class MessageDisplayer {

    private static MessageDisplayer instance;
    private AppCompatActivity currentActivity;
    private Displayable display;

    public void displayMessage(String msg) {
        display.display(msg);
    }

    public void setDisplay(Displayable display) {
        this.display = display;
    }

    public void setCurrentActivity(AppCompatActivity currentActivity) {
        this.currentActivity = currentActivity;
    }

    public static MessageDisplayer getInstance() {
        return instance == null ? instance = new MessageDisplayer() : instance;
    }

    private interface Displayable {
        void display(String msg);
    }

}
