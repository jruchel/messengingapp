package jruchel.messengingapp.Networking;

import java.util.Random;

import jruchel.messengingapp.Utils.MessageDisplayer;

public class Matcher implements Runnable {

    private static Matcher instance;
    private Runnable matchFoundListener;
    private MatchResult matchResult;

    public static Matcher getInstance() {
        return instance == null ? instance = new Matcher() : instance;
    }

    public void setMatchFoundListener(Runnable matchFoundListener) {
        this.matchFoundListener = matchFoundListener;
    }

    @Override
    public void run() {
        //Matching...

        Random random = new Random();

        switch (random.nextInt() % 2) {
            case 0:
                matchResult = MatchResult.FOUND;
            default:
                matchResult = MatchResult.NOT_FOUND;
        }

        switch (matchResult) {
            case FOUND:
                matchFoundListener.run();
                break;
            case NOT_FOUND:
                MessageDisplayer.getInstance().displayMessage("Match not found");
                break;
        }

    }

    public enum MatchResult {
        FOUND, NOT_FOUND;
    }
}
