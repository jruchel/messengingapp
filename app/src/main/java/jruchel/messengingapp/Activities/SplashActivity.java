package jruchel.messengingapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import jruchel.messengingapp.Networking.Matcher;
import jruchel.messengingapp.R;
import jruchel.messengingapp.Resources.UserData;

public class SplashActivity extends AppCompatActivity {

    //Views
    private EditText nameInput;
    private Button okButton;

    //Variables
    private String name;

    //Singletons
    private Matcher matcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        matcher = Matcher.getInstance();

        matcher.setMatchFoundListener(new Runnable() {
            @Override
            public void run() {
                startNextActivity();
            }
        });
    }

    private void initViews() {
        nameInput = findViewById(R.id.nameInput);
        okButton = findViewById(R.id.okButton);

        nameInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                name = nameInput.getText().toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserData.getInstance().setUsername(name);
            }
        });
    }

    private void startNextActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void startMatching() {
        //Looking into the database and trying to find a match
        matcher.run();
    }


}
