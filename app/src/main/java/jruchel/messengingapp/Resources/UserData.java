package jruchel.messengingapp.Resources;

public class UserData {

    private String username;
    private static UserData instance;

    public static UserData getInstance() {
        return instance == null ? instance = new UserData() : instance;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
